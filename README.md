# myslow

A tool for mysql query-slow.log dump.

```
go get -u -v gitee.com/lanyue/myslow@v0.1.0
```

```sh
# Output logs data to json file
myslow -out ./data.json /data/mysql/logs/query-slow.log

# Insert logs data to mysql table
myslow -dsn="root:123456@tcp(localhost:3306)/test?charset=utf8mb4&parseTime=true&loc=Local" --table="slow_logs" /data/mysql/query-slow.log
```
